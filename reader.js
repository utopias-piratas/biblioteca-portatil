---
---

{% node_module zepto/dist/zepto.min.js %}
{% node_module jszip/dist/jszip.min.js %}
{% node_module epubjs/dist/epub.min.js %}

// Esperar hasta que termine de cargar toda la página para ejecutar el
// JS
$(document).ready(() => {
  // Encontrar la dirección del libro del parámetro book, por ejemplo
  // http://biblio.box/reader.html?book=manifiesto_telecomunista.epub
  var query = new URLSearchParams(window.location.search);
  var url = query.get('book');
  // Acá va la dirección del libro, que vamos a tomar de la URL
  var book = window.book = ePub(url);
  var render = window.render = book.renderTo('reader', { flow: 'paginated' });
  var display = render.display();

  // Esta función agrega swipe.js dentro del iframe del epub
  render.hooks.render.register(view => {
    // Agregar la ubicación del swipe junto con la dirección actual
    // porque epub.js la carga relativa al libro.
    //
    // TODO de todas formas depende de la estructura de /reader/ con lo
    // que no es realmente relativa
    view.contents.addScript(window.location.pathname+'../swipe.js');
  });

  // Ahora hay que procesar los eventos
  // aca cambie la sintaxis de js
  book.ready.then(() => {
    // Esto hace que podamos cambiar de página usando las flechas del
    // teclado
    var keyListener = function(e){
      // Left Key
      if ((e.keyCode || e.which) == 37) {
        book.package.metadata.direction === "rtl" ? render.next() : render.prev();
      }

      // Right Key
      if ((e.keyCode || e.which) == 39) {
        book.package.metadata.direction === "rtl" ? render.prev() : render.next();
      }

    };

    render.on('keyup', keyListener);
    $(document).on('keyup', keyListener);
  });
});
