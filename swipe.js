---
---

{% node_module hammerjs/hammer.min.js %}

var book = parent.book,
    render = parent.render;

var hammer = new Hammer.Manager(window, {});

var singleTap = new Hammer.Tap({ event: 'singletap' });
var doubleTap = new Hammer.Tap({ event: 'doubletap', taps: 2 });

hammer.add([doubleTap, singleTap]);

doubleTap.recognizeWith(singleTap);
singleTap.requireFailure(doubleTap);

hammer.on('doubletap swiperight', (ev) => {
  book.package.metadata.direction === "rtl" ? render.next() : render.prev();
});

hammer.on('singletap swipeleft', (ev) => {
  book.package.metadata.direction === "rtl" ? render.prev() : render.next();
});
