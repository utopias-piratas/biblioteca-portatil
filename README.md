Sitio de demostración de
[jekyll-calibre](https://0xacab.org/utopias-piratas/jekyll-calibre)

Basado en [Webjeda Cards](http://webjeda.com/cards)

# Instalación

* Clonar este repositorio
* Configurar la biblioteca de Calibre en `_config.yml`
* Instalar las gemas:

```bash
bundle
```

* Compilar el sitio:

```bash
bundle exec jekyll build
```

* Copiar `_site` al servidor web
